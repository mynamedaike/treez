# Simple Order Service
This order REST API service is built in Java. 

## Install the Dependencies

- JDK (1.8.0_201)
- Spring Boot (1.5.6.RELEASE)
- Tomcat (8.5.39)
- MySQL (8.0.13)
