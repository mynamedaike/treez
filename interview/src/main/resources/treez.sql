-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: treez
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `inventory` (
  `product_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Product Id',
  `product_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Product Name',
  `product_description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Product Description',
  `product_price` double(16,2) NOT NULL COMMENT 'Product Price',
  `product_stock` int(4) NOT NULL COMMENT 'Product Stock',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time when the data was created',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time when the data was updated lastly',
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES (1,'Bacon & Egger','A freshly cracked egg from hens fed a vegetarian diet, topped with real cheddar cheese and naturally smoked bacon from pork raised without the use of antibiotics, served on a toasted English Muffin or Sesame seed bun.',4.39,978,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(2,'Sausage & Egger','A freshly cracked egg from hens fed a vegetarian diet, topped with processed cheddar and a mouth-watering sausage patty from pork raised without the use of antibiotics, served on a toasted English Muffin or Sesame seed bun.',4.39,969,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(3,'Cheese & Egger','A freshly cracked egg from hens fed a vegetarian diet, topped with real cheddar cheese and served on a toasted English Muffin or Sesame seed bun.',3.49,961,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(4,'Breakfast Wrap','A freshly cracked egg from hens fed a vegetarian diet, topped with real cheddar cheese, naturally smoked bacon from pork raised without the use of antibiotics and ketchup, wrapped in a flour tortilla.',2.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(5,'Hash Brown','Our golden-brown hash browns are made from the finest potatoes and cooked just the way you like them – fluffy on the inside and crispy on the outside.',1.99,998,'2020-01-27 06:53:40','2020-01-27 08:13:57'),(6,'Teen Burger','A perfectly seasoned 100% pure beef patty – raised without the use of hormones or steroids – topped with processed cheddar cheese, mouth-watering bacon from pork raised without the use of antibiotics, crisp lettuce, onion, tomato, pickles, ketchup, mustard and Teen® sauce, served on a freshly toasted sesame seed bun.',6.69,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(7,'Double Teen Burger','Two perfectly seasoned patties of 100% pure beef – raised without artificial hormones and steroids – topped with real cheddar cheese, mouth-watering bacon from pork raised without the use of antibiotics, crisp lettuce, tomato, pickles and Teen® sauce, served on a freshly toasted bun.',8.49,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(8,'Mozza Burger','A perfectly seasoned patty of 100% pure beef – raised without artificial hormones and steroids – topped with real mozzarella cheese, mouth-watering bacon from pork raised without the use of antibiotics, crisp lettuce, tomato and Mozza® sauce, served on a freshly toasted sesame seed bun.',6.69,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(9,'Double Mozza Burger','Two perfectly seasoned patties of 100% pure beef – raised without artificial hormones and steroids – topped with real mozzarella cheese, mouth-watering bacon from pork raised without the use of antibiotics, crisp lettuce, tomato and Mozza® sauce, served on a freshly toasted bun.',8.49,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(10,'Mama Burger','A perfectly seasoned patty of 100% pure beef – raised without artificial hormones and steroids – topped with pickles, a slice of onion, ketchup, mustard and Teen® sauce, served on a freshly toasted sesame seed bun.',5.29,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(11,'Papa Burger','Two perfectly seasoned patties of 100% pure beef – raised without artificial hormones and steroids – topped with pickles, a slice of onion, ketchup, mustard and Teen® sauce, served on a freshly toasted sesame seed bun.',7.09,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(12,'Grandpa Burger','Three perfectly seasoned patties of 100% pure beef – raised without artificial hormones and steroids – topped with pickles, a slice of onion, ketchup, mustard and Teen® sauce, served on a freshly toasted sesame seed bun.',8.89,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(13,'Cheddar Bacon Uncle Burger','A perfectly seasoned 5 oz patty of 100% pure beef – raised without artificial hormones and steroids – topped with real cheddar cheese, mouth-watering bacon from pork raised without the use of antibiotics, red onion, crisp lettuce, pickles and a tomato, served on a freshly toasted sesame seed bun.',8.99,998,'2020-01-27 06:53:40','2020-01-27 08:13:57'),(14,'Buddy Burger','A perfectly seasoned 1.6 oz patty of 100% pure beef – raised without artificial hormones and steroids – topped with freshly grilled onions, ketchup, mustard and Teen® sauce, served on a freshly toasted bun.',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(15,'Double Buddy Burger','Two perfectly seasoned 1.6 oz patties of 100% pure beef – raised without artificial hormones and steroids – topped with freshly grilled onions, ketchup, mustard and Teen® sauce, served on a freshly toasted bun.',2.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(16,'Chubby Chicken Burger','Deliciously seasoned 100% chicken breast – from chickens raised without the use of antibiotics – battered and breaded to perfection with toasted wheat crumbs, wheat flour and spices, topped with mayo and crisp lettuce, served on a freshly toasted sesame seed bun.',6.69,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(17,'Spicy Habanero Chicken Burger','Deliciously seasoned 100% chicken breast – from chickens raised without the use of antibiotics – battered and breaded to perfection with toasted wheat crumbs, wheat flour and spices, flavoured with cayenne pepper, jalapeño pepper and habanero chilli, topped with red jalapeño aioli, crisp lettuce and tomato, served on a freshly toasted sesame seed bun.',6.99,998,'2020-01-27 06:53:40','2020-01-27 08:17:52'),(18,'3 Chicken Strips','3 Chicken Strips, made from 100% chicken breast from chickens raised without the use of antibiotics.',99.00,998,'2020-01-27 06:53:40','2020-01-27 08:17:52'),(19,'5 Chicken Strips','5 Chicken Strips, made from 100% chicken breast from chickens raised without the use of antibiotics.',8.49,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(20,'A&W Root Beer','Pop drink',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(21,'Coca-Cola','Pop drink',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(22,'Sprite','Pop drink',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(23,'Iced Tea','Pop drink',1.99,998,'2020-01-27 06:53:40','2020-01-27 08:17:52'),(24,'Orange Juice','Orange Juice',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(25,'Apple Juice','Apple Juice',1.99,999,'2020-01-27 06:53:40','2020-01-27 06:53:40'),(26,'Coffee','Coffee',1.59,998,'2020-01-27 06:53:40','2020-01-27 08:13:57'),(27,'Tea','Tea',1.59,999,'2020-01-27 06:53:40','2020-01-27 06:53:40');
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_detail` (
  `detail_id` varchar(32) NOT NULL COMMENT 'The order detail id',
  `order_id` varchar(32) NOT NULL COMMENT 'The order id',
  `product_id` int(4) NOT NULL COMMENT 'The product id',
  `product_name` varchar(32) NOT NULL COMMENT 'The product name',
  `product_price` double(16,2) NOT NULL COMMENT 'The product price',
  `product_quantity` int(4) NOT NULL COMMENT 'The product quantity',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time when the order detail was created',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time when the order detail was updated lastly',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES ('1580141637915813057','1580141637915868423',5,'Hash Brown',1.99,1,'2020-01-27 08:13:58','2020-01-27 08:13:58'),('1580141637933468997','1580141637915868423',26,'Coffee',1.59,1,'2020-01-27 08:13:58','2020-01-27 08:13:58'),('1580141637937124223','1580141637915868423',13,'Cheddar Bacon Uncle Burger',8.99,1,'2020-01-27 08:13:58','2020-01-27 08:13:58'),('1580141872568570548','1580141872568596504',17,'Spicy Habanero Chicken Burger',6.99,1,'2020-01-27 08:17:53','2020-01-27 08:17:53'),('1580141872584707994','1580141872568596504',18,'3 Chicken Strips',99.00,1,'2020-01-27 08:17:53','2020-01-27 08:17:53'),('1580141872586112151','1580141872568596504',23,'Iced Tea',1.99,1,'2020-01-27 08:17:53','2020-01-27 08:17:53');
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_master`
--

DROP TABLE IF EXISTS `order_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_master` (
  `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'The order id',
  `email_address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'The customer email address',
  `order_amount` double(16,2) NOT NULL COMMENT 'The total amount of products',
  `order_status` int(4) NOT NULL COMMENT 'The status of the order',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time when the order was created',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time when the order was updated lastly',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_master`
--

LOCK TABLES `order_master` WRITE;
/*!40000 ALTER TABLE `order_master` DISABLE KEYS */;
INSERT INTO `order_master` VALUES ('1580141637915868423','bill@123.com',12.57,0,'2020-01-27 08:13:58','2020-01-27 08:13:58'),('1580141872568596504','bill@123.com',107.98,0,'2020-01-27 08:17:53','2020-01-27 08:17:53');
/*!40000 ALTER TABLE `order_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-27  8:57:58
