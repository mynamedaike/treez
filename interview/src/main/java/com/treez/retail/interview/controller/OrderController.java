package com.treez.retail.interview.controller;

import com.treez.retail.interview.dto.OrderDTO;
import com.treez.retail.interview.response.ResponseVO;
import com.treez.retail.interview.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName OrderController
 * @Description OrderController
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RestController
@RequestMapping("orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping()
    public ResponseVO createOrder(@RequestParam("emailAddress") String emailAddress,
                                  @RequestBody List<HashMap<String, Integer>> productList) {
        HashMap<Integer, Integer> cart = new HashMap<>();
        for(HashMap<String, Integer> map: productList) {
            cart.put(map.get("id"), map.get("quantity"));
        }
        OrderDTO orderDTO = orderService.create(emailAddress, cart);
        return  ResponseVO.success(orderDTO);
    }

    @GetMapping()
    public ResponseVO getAllOrdersByPage(@RequestParam("pageIndex") Integer pageIndex,
                                         @RequestParam("pageSize") Integer pageSize) {
        Page<OrderDTO> page = orderService.findAll(new PageRequest(pageIndex - 1, pageSize));
        HashMap<String, Object> map = new HashMap<>();
        map.put("totalNum", page.getTotalElements());
        map.put("list", page.getContent());
        return ResponseVO.success(map);
    }

    @GetMapping("/1")
    public ResponseVO getOneOrderById(@RequestParam("orderId") String orderId) {
        OrderDTO orderDTO = orderService.findOne(orderId);
        return ResponseVO.success(orderDTO);
    }

    @PutMapping("/1")
    public ResponseVO updateOneOrderById(@RequestParam("orderId") String orderId,
                                         @RequestBody List<HashMap<String, Integer>> productList) {
        HashMap<Integer, Integer> cart = new HashMap<>();
        for(HashMap<String, Integer> map: productList) {
            cart.put(map.get("id"), map.get("quantity"));
        }
        OrderDTO orderDTO = orderService.update(orderId, cart);
        return ResponseVO.success(orderDTO);
    }

    @PutMapping("/1/cancel")
    public ResponseVO cancelOneOrder(@RequestParam("orderId") String orderId) {
        orderService.cancel(orderId);
        return ResponseVO.success(null);
    }

    @DeleteMapping("/1")
    public ResponseVO deleteOneOrder(@RequestParam("orderId") String orderId){
        orderService.delete(orderId);
        return ResponseVO.success(null);
    }
}

