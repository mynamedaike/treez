package com.treez.retail.interview.service;

import com.treez.retail.interview.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;

/**
 * @ClassName OrderService
 * @Description OrderService
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public interface OrderService {

    OrderDTO create(String emailAddress, HashMap<Integer, Integer> cart);

    Page<OrderDTO> findAll(Pageable pageable);

    OrderDTO findOne(String orderId);

    OrderDTO update(String orderId, HashMap<Integer, Integer> cart);

    void cancel(String orderId);

    void delete(String orderId);
}
