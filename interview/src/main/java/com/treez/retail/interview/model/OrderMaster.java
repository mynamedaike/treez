package com.treez.retail.interview.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @ClassName OrderMaster
 * @Description OrderMaster
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Entity
@Data
@EnableJpaAuditing
public class OrderMaster {

    @Id
    private String orderId;

    private String emailAddress;

    private Double orderAmount;

    private Integer orderStatus;

    @CreationTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @UpdateTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public OrderMaster() {
    }

    public OrderMaster(String orderId, String emailAddress, Double orderAmount, Integer orderStatus) {
        this.orderId = orderId;
        this.emailAddress = emailAddress;
        this.orderAmount = orderAmount;
        this.orderStatus = orderStatus;
    }
}
