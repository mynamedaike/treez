package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName OrderMasterDAO
 * @Description OrderMasterDAO
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public interface OrderMasterDAO extends JpaRepository<OrderMaster, String> {
}
