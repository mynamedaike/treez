package com.treez.retail.interview.util;

import java.util.Random;

/**
 * @ClassName PrimaryKeyUtil
 * @Description PrimaryKeyUtil
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public class PrimaryKeyUtil {

    public static synchronized String generate() {
        Long currentTimestamp = System.currentTimeMillis();
        Integer randomNum = new Random().nextInt(900000) + 100000;
        return String.valueOf(currentTimestamp) + randomNum;
    }
}
