package com.treez.retail.interview.service;

import com.treez.retail.interview.model.Inventory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName InventoryService
 * @Description InventoryService
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public interface InventoryService {
    Inventory save(Inventory inventory);

    Page<Inventory> findAll(Pageable pageable);

    List<Inventory> findAll();

    Inventory findOne(Integer id);

    void delete(Integer id);

    void increaseStock(HashMap<Integer, Integer> cart);

    void decreaseStock(HashMap<Integer, Integer> cart);
}
