package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName InventoryDAO
 * @Description InventoryDAO
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public interface InventoryDAO extends JpaRepository<Inventory, Integer> {
}
