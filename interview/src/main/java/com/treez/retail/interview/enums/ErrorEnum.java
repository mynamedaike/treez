package com.treez.retail.interview.enums;

import lombok.Getter;

/**
 * @ClassName ErrorEnum
 * @Description ErrorEnum
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Getter
public enum ErrorEnum {
    UNKNOWN_ERROR(-1, "An unknown error happened!"),
    REQUEST_PARAM_ERROR(10, "The request parameters are not correct!"),
    //    DATA_NOT_EXIST(20, "The request data does not exist!"),
    PRODUCT_NOT_EXIST(30, "The product does not exist!"),
    PRODUCT_STOCK_INSUFFICIENT(40, "The product stock is insufficient!"),
    ORDER_NOT_EXSIT(50, "The queried order does not exist!"),
    ORDER_ALREADY_CANELLED(60, "The order has already been cancelled!"),
    ORDER_OPERATION_NOT_ALLOWD(70, "The current order status does not allow to execute this operation!");

    private Integer code;

    private String msg;

    ErrorEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
