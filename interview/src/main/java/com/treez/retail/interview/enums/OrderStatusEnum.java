package com.treez.retail.interview.enums;

import lombok.Getter;

/**
 * @ClassName OrderStatusEnum
 * @Description OrderStatusEnum
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Getter
public enum OrderStatusEnum {
    //The order is created by the customer.
    CREATED(0, "Created"),
    //The order is received by the seller.
    RECEIVED(1, "Received"),
    //The order is cancelled by the customer or the seller.
    CANCELLED(2, "Cancelled"),
    //The products are in transit.
    TRANSIT(3, "In Transit"),
    //The products are delivered.
    DELIVERED(4, "Delivered");

    private Integer code;

    private String msg;

    OrderStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static String getMsgByCode(Integer code) {
        for(OrderStatusEnum orderStatusEnum: OrderStatusEnum.values()) {
            if(orderStatusEnum.getCode() == code) {
                return orderStatusEnum.getMsg();
            }
        }
        return null;
    }
}