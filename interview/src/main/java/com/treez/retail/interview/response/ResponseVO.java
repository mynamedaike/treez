package com.treez.retail.interview.response;

import lombok.Data;

/**
 * @ClassName ResponseVO
 * @Description ResponseVO
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Data
public class ResponseVO<T> {
    private Integer id;

    private String msg;

    private T data;

    public ResponseVO(Integer id, String msg, T data) {
        this.id = id;
        this.msg = msg;
        this.data = data;
    }

    public static ResponseVO success(Object data) {
        ResponseVO<Object> responseVO = new ResponseVO<>(0, "success", data);
        return responseVO;
    }

    public static ResponseVO failure(Integer id, String msg) {
        ResponseVO<Object> responseVO = new ResponseVO<>(id, msg, null);
        return responseVO;
    }
}
