package com.treez.retail.interview.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @ClassName OrderDetail
 * @Description OrderDetail
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Entity
@Data
@EnableJpaAuditing
public class OrderDetail {

    @Id
    private String detailId;

    private String orderId;

    private Integer productId;

    private String productName;

    private Double productPrice;

    private Integer productQuantity;

    @CreationTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @UpdateTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public OrderDetail() {
    }

    public OrderDetail(String detailId, String orderId, Integer productId, String productName,
                       Double productPrice, Integer productQuantity) {
        this.detailId = detailId;
        this.orderId = orderId;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQuantity = productQuantity;
    }

    public OrderDetail(String detailId, String orderId, Inventory inventory, Integer productQuantity) {
        this.detailId = detailId;
        this.orderId = orderId;
        this.productId = inventory.getProductId();
        this.productName = inventory.getProductName();
        this.productPrice = inventory.getProductPrice();
        this.productQuantity = productQuantity;
    }
}
