package com.treez.retail.interview.exception;

/**
 * @ClassName OrderException
 * @Description OrderException
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */

import com.treez.retail.interview.enums.ErrorEnum;
import lombok.Getter;
@Getter
public class OrderException extends RuntimeException {

    private Integer code;

    public OrderException(ErrorEnum errorEnum) {
        super(errorEnum.getMsg());
        this.code = errorEnum.getCode();
    }
}
