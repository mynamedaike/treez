package com.treez.retail.interview.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName TestController
 * @Description TestController
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RestController
public class TestController {

    @GetMapping("/")
    public String print() {
        return "Hello!";
    }
}
