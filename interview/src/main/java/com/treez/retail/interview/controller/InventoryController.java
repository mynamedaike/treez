package com.treez.retail.interview.controller;

import com.treez.retail.interview.enums.ErrorEnum;
import com.treez.retail.interview.exception.OrderException;
import com.treez.retail.interview.model.Inventory;
import com.treez.retail.interview.response.ResponseVO;
import com.treez.retail.interview.service.InventoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @ClassName InventoryController
 * @Description InventoryController
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RestController
@RequestMapping("/inventories")
public class InventoryController {

    @Autowired
    private InventoryServiceImpl inventoryService;

    @PostMapping()
    public ResponseVO createProduct(@RequestParam("productName") String productName,
                                    @RequestParam("productDescription") String productDescription,
                                    @RequestParam("productPrice") Double productPrice,
                                    @RequestParam("productStock") Integer productStock) {

        Inventory inventory = new Inventory(productName, productDescription, productPrice, productStock);
        Integer inventoryId = inventoryService.save(inventory).getProductId();
        return ResponseVO.success(inventoryId);
    }

    @GetMapping()
    public ResponseVO getAllProductsByPage(@RequestParam("pageIndex") Integer pageIndex,
                                           @RequestParam("pageSize") Integer pageSize) {
        Page<Inventory> page = inventoryService.findAll(new PageRequest(pageIndex - 1, pageSize));
        HashMap<String, Object> map = new HashMap<>();
        map.put("totalNum", page.getTotalElements());
        map.put("list", page.getContent());
        return ResponseVO.success(map);
    }

    @GetMapping("/1")
    public ResponseVO getOneProductById(@RequestParam("productId") Integer productId) {
        Inventory inventory = inventoryService.findOne(productId);
        return ResponseVO.success(inventory);
    }

    @PutMapping("/1")
    public ResponseVO updateOneProduct(@RequestParam("productId") Integer productId,
                                       @RequestParam(value = "productName", required = false) String productName,
                                       @RequestParam(value = "productDescription", required = false) String productDescription,
                                       @RequestParam(value = "productPrice", required = false) Double productPrice,
                                       @RequestParam(value = "productStock", required = false) Integer productStock) {
        Inventory inventory = inventoryService.findOne(productId);
        if(inventory == null) {
            throw new OrderException(ErrorEnum.PRODUCT_NOT_EXIST);
        }
        if(productName == null && productDescription == null && productPrice == null && productStock == null) {
            throw new OrderException(ErrorEnum.REQUEST_PARAM_ERROR);
        }
        if(productName != null) {
            inventory.setProductName(productName);
        }
        if(productDescription != null) {
            inventory.setProductDescription(productDescription);
        }
        if(productPrice != null) {
            inventory.setProductPrice(productPrice);
        }
        if(productStock != null) {
            inventory.setProductStock(productStock);
        }
        inventory = inventoryService.save(inventory);
        return ResponseVO.success(inventory);
    }

    @DeleteMapping("/1")
    public ResponseVO deleteOneProduct(@RequestParam("productId") Integer productId) {
        inventoryService.delete(productId);
        return ResponseVO.success(null);
    }
}
