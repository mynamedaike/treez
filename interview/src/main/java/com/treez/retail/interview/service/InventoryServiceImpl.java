package com.treez.retail.interview.service;

import com.treez.retail.interview.dao.InventoryDAO;
import com.treez.retail.interview.enums.ErrorEnum;
import com.treez.retail.interview.exception.OrderException;
import com.treez.retail.interview.model.Inventory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName InventoryServiceImpl
 * @Description InventoryServiceImpl
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Service
@Transactional
@Slf4j
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryDAO inventoryDAO;

    @Override
    public Inventory save(Inventory inventory) {
        return inventoryDAO.save(inventory);
    }

    @Override
    public Page<Inventory> findAll(Pageable pageable) {
        return inventoryDAO.findAll(pageable);
    }

    @Override
    public List<Inventory> findAll() {
        return inventoryDAO.findAll();
    }

    @Override
    public Inventory findOne(Integer id) {
        Inventory inventory = inventoryDAO.findOne(id);
        if(inventory == null) {
            throw new OrderException(ErrorEnum.PRODUCT_NOT_EXIST);
        }
        return inventory;
//        return inventoryDAO.findOne(id);
    }

    @Override
    public void delete(Integer id) {
        inventoryDAO.delete(id);
    }

    @Override
    public synchronized void increaseStock(HashMap<Integer, Integer> cart) {
        Inventory inventory;
        for(Integer productId: cart.keySet()) {
            inventory = inventoryDAO.findOne(Integer.valueOf(productId));
            inventory.setProductStock(inventory.getProductStock() + cart.get(productId));
            inventoryDAO.save(inventory);
        }
    }

    @Override
    public synchronized void decreaseStock(HashMap<Integer, Integer> cart) {
        Inventory inventory;
        for(Integer productId: cart.keySet()) {
            inventory = inventoryDAO.findOne(Integer.valueOf(productId));
            if(inventory.getProductStock() < cart.get(productId)) {
                log.error("[STOCK INSUFFICIENT] The stock of {} is {}, but the quantity of {} in the cart is {}.",
                        inventory.getProductName(), inventory.getProductStock(), inventory.getProductName(),
                        cart.get(productId));
                throw new OrderException(ErrorEnum.PRODUCT_STOCK_INSUFFICIENT);
            }
            inventory.setProductStock(inventory.getProductStock() - cart.get(productId));
            inventoryDAO.save(inventory);
        }
    }
}
