package com.treez.retail.interview.service;

import com.treez.retail.interview.dao.OrderDetailDAO;
import com.treez.retail.interview.dao.OrderMasterDAO;
import com.treez.retail.interview.dto.OrderDTO;
import com.treez.retail.interview.enums.ErrorEnum;
import com.treez.retail.interview.enums.OrderStatusEnum;
import com.treez.retail.interview.exception.OrderException;
import com.treez.retail.interview.model.Inventory;
import com.treez.retail.interview.model.OrderDetail;
import com.treez.retail.interview.model.OrderMaster;
import com.treez.retail.interview.util.PrimaryKeyUtil;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName OrderServiceImpl
 * @Description OrderServiceImpl
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMasterDAO orderMasterDAO;

    @Autowired
    private OrderDetailDAO orderDetailDAO;

    @Autowired
    private InventoryService inventoryService;

    private Pair<Double, List<OrderDetail>> getOrderDetailListAndOrderAmount(String orderId, HashMap<Integer, Integer> cart) {
        List<OrderDetail> orderDetailList = new ArrayList<>();
        Double amount = 0.0;

        Inventory inventory;
        OrderDetail orderDetail;

        for(Integer productId: cart.keySet()) {
            String detailId = PrimaryKeyUtil.generate();
            inventory = inventoryService.findOne(productId);

            if(inventory == null) {
                throw new OrderException(ErrorEnum.PRODUCT_NOT_EXIST);
            }

            orderDetail = new OrderDetail(detailId, orderId, inventory, cart.get(productId));
            orderDetail = orderDetailDAO.save(orderDetail);
            orderDetailList.add(orderDetail);

            amount += inventory.getProductPrice() * cart.get(productId);
        }
        Pair<Double, List<OrderDetail>> pair = new Pair<>(amount, orderDetailList);
        return pair;
    }

    private void checkOrderStatus(OrderMaster orderMaster) {
        if (orderMaster == null) {
            throw new OrderException(ErrorEnum.ORDER_NOT_EXSIT);
        }
        if (orderMaster.getOrderStatus() == OrderStatusEnum.CANCELLED.getCode()) {
            throw new OrderException((ErrorEnum.ORDER_ALREADY_CANELLED));
        }
        if (orderMaster.getOrderStatus() == OrderStatusEnum.TRANSIT.getCode() ||
                orderMaster.getOrderStatus() == OrderStatusEnum.DELIVERED.getCode()) {
            throw new OrderException((ErrorEnum.ORDER_OPERATION_NOT_ALLOWD));
        }
    }

    private void deleteOrderDetail(String orderId) {
        List<OrderDetail> orderDetailList = orderDetailDAO.findByOrderId(orderId);
        HashMap<Integer, Integer> cart = new HashMap<>();
        for(OrderDetail orderDetail: orderDetailList) {
            cart.put(orderDetail.getProductId(), orderDetail.getProductQuantity());
            orderDetailDAO.delete(orderDetail);
        }
        inventoryService.increaseStock(cart);
    }

    @Override
    public OrderDTO create(String emailAddress, HashMap<Integer, Integer> cart) {
        String orderId = PrimaryKeyUtil.generate();

        //1. Write the orderDetail to the database, and get the orderDetailList and the order amount.
        Pair<Double, List<OrderDetail>> pair = getOrderDetailListAndOrderAmount(orderId, cart);
        Double amount = pair.getKey();
        List<OrderDetail> orderDetailList = pair.getValue();

        //2. Write the orderMaster to the database.
        OrderMaster orderMaster = new OrderMaster(orderId, emailAddress, amount, OrderStatusEnum.CREATED.getCode());
        orderMaster = orderMasterDAO.saveAndFlush(orderMaster);

        //3. Decrease the product stock.
        inventoryService.decreaseStock(cart);

        //4. Return the OrderDTO.
        OrderDTO orderDTO = new OrderDTO(orderMaster, orderDetailList);
        return orderDTO;
    }

    @Override
    public Page<OrderDTO> findAll(Pageable pageable) {
        List<OrderDTO> orderDTOList = new ArrayList<>();
        Page<OrderMaster> page = orderMasterDAO.findAll(pageable);
        for(OrderMaster orderMaster: page.getContent()) {
            orderDTOList.add(new OrderDTO(orderMaster, orderDetailDAO.findByOrderId(orderMaster.getOrderId())));
        }
        Page<OrderDTO> orderDTOPage = new PageImpl<>(orderDTOList, pageable, page.getTotalElements());
        return orderDTOPage;
    }

    @Override
    public OrderDTO findOne(String orderId) {
        OrderMaster orderMaster = orderMasterDAO.findOne(orderId);
        if(orderMaster == null) {
            throw new OrderException(ErrorEnum.ORDER_NOT_EXSIT);
        }
        List<OrderDetail> orderDetailList = orderDetailDAO.findByOrderId(orderId);
        OrderDTO orderDTO = new OrderDTO(orderMaster, orderDetailList);
        return orderDTO;
    }

    @Override
    public OrderDTO update(String orderId, HashMap<Integer, Integer> cart) {
        //1. Check if the order exists and if the order status allows to update the order.
        OrderMaster orderMaster = orderMasterDAO.findOne(orderId);
        checkOrderStatus(orderMaster);

        //2. Delete the related order detail data
        deleteOrderDetail(orderId);

        //3. Write the new orderDetail to the database, and get the new orderDetailList and the order amount.
        Pair<Double, List<OrderDetail>> pair = getOrderDetailListAndOrderAmount(orderId, cart);
        Double amount = pair.getKey();
        List<OrderDetail> orderDetailList = pair.getValue();

        //4. Write the orderMaster to the database. need to change the order status?
        orderMaster.setOrderAmount(amount);
        orderMaster = orderMasterDAO.save(orderMaster);

        //5. Decrease the product stock.
        inventoryService.decreaseStock(cart);

        //6. Return the OrderDTO.
        OrderDTO orderDTO = new OrderDTO(orderMaster, orderDetailList);
        return orderDTO;
    }

    @Override
    public void cancel(String orderId) {
        OrderMaster orderMaster = orderMasterDAO.findOne(orderId);
        checkOrderStatus(orderMaster);
        orderMaster.setOrderStatus(OrderStatusEnum.CANCELLED.getCode());

        List<OrderDetail> orderDetailList = orderDetailDAO.findByOrderId(orderId);
        HashMap<Integer, Integer> cart = new HashMap<>();
        for(OrderDetail orderDetail: orderDetailList) {
            cart.put(orderDetail.getProductId(), orderDetail.getProductQuantity());
        }
        inventoryService.increaseStock(cart);
    }

    @Override
    public void delete(String orderId) {
        OrderMaster orderMaster = orderMasterDAO.findOne(orderId);
        checkOrderStatus(orderMaster);
        orderMasterDAO.delete(orderId);
        deleteOrderDetail(orderId);
    }
}
