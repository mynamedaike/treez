package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @ClassName OrderDetailDAO
 * @Description OrderDetailDAO
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
public interface OrderDetailDAO extends JpaRepository<OrderDetail, String> {
    List<OrderDetail> findByOrderId(String orderId);
}
