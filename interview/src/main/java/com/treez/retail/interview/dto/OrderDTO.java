package com.treez.retail.interview.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.treez.retail.interview.enums.OrderStatusEnum;
import com.treez.retail.interview.model.OrderDetail;
import com.treez.retail.interview.model.OrderMaster;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName OrderDTO
 * @Description OrderDTO
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@Data
public class OrderDTO {

    private String orderId;

    private String emailAddress;

    private Double orderAmount;

    private String orderStatus;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private List<OrderDetail> orderDetailList = new ArrayList<>();

    public OrderDTO() {
    }

    public OrderDTO(String orderId, String emailAddress, Double orderAmount, String orderStatus,
                    Date createTime, Date updateTime, List<OrderDetail> orderDetailList) {
        this.orderId = orderId;
        this.emailAddress = emailAddress;
        this.orderAmount = orderAmount;
        this.orderStatus = orderStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.orderDetailList = orderDetailList;
    }

    public OrderDTO(OrderMaster orderMaster, List<OrderDetail> orderDetailList) {
        this.orderId = orderMaster.getOrderId();
        this.emailAddress = orderMaster.getEmailAddress();
        this.orderAmount = orderMaster.getOrderAmount();
        this.orderStatus = OrderStatusEnum.getMsgByCode(orderMaster.getOrderStatus());
        this.createTime = orderMaster.getCreateTime();
        this.updateTime = orderMaster.getUpdateTime();
        this.orderDetailList = orderDetailList;
    }
}

