package com.treez.retail.interview.exception;

import com.treez.retail.interview.enums.ErrorEnum;
import com.treez.retail.interview.response.ResponseVO;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName ExceptionHandler
 * @Description ExceptionHandler
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseVO handler(Exception e) {
        ResponseVO responseVO;
        if(e instanceof OrderException) {
            responseVO = ResponseVO.failure(((OrderException) e).getCode(), e.getMessage());
        }else {
            responseVO = ResponseVO.failure(ErrorEnum.UNKNOWN_ERROR.getCode(), e.getMessage());
        }
        return responseVO;
    }
}
