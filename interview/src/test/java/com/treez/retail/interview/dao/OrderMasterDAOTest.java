package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.OrderMaster;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

/**
 * @ClassName OrderMasterDAOTest
 * @Description OrderMasterDAOTest
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class OrderMasterDAOTest {

    @Autowired
    private OrderMasterDAO orderMasterDAO;

    @Test
    public void saveTest() {
        OrderMaster orderMaster = new OrderMaster("12345683", "William@123.com",
                65.43, 4);
        orderMaster = orderMasterDAO.save(orderMaster);
        Assertions.assertEquals(65.43, orderMaster.getOrderAmount());
    }

    @Test
    public void findAllTest() {
        PageRequest pageRequest = new PageRequest(0, 2);
        Page<OrderMaster> page = orderMasterDAO.findAll(pageRequest);
        Assertions.assertEquals(2, page.getTotalElements());
        Assertions.assertEquals(107.98, page.getContent().get(1).getOrderAmount());
        Assertions.assertEquals(2, orderMasterDAO.findAll().size());
    }

    @Test
    public void findOneTest() {
        OrderMaster orderMaster = orderMasterDAO.findOne("1580141637915868423");
        Assertions.assertEquals(12.57, orderMaster.getOrderAmount());
    }

    @Test
    public void deleteTest() {
        orderMasterDAO.delete("1580141872568596504");
        Assertions.assertNull(orderMasterDAO.findOne("1580141872568596504"));
    }
}