package com.treez.retail.interview.service;

import com.treez.retail.interview.model.Inventory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName InventoryServiceTest
 * @Description InventoryServiceTest
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class InventoryServiceTest {

    @Autowired
    private InventoryServiceImpl inventoryService;

    @Test
    public void saveTest() {
        Inventory inventory = new Inventory("2% Milk", "2% Milk",
                2.99, 999);
        inventory = inventoryService.save(inventory);
        Assertions.assertEquals("2% Milk", inventory.getProductName());

        inventory = inventoryService.findOne(19);
        inventory.setProductPrice(9.99);
        inventory = inventoryService.save(inventory);
//        inventory = inventoryService.findOne(19);
        Assertions.assertEquals(9.99, inventory.getProductPrice());
    }

    @Test
    public void findAllTest() {
        PageRequest pageRequest = new PageRequest(0,10);
        Page<Inventory> page = inventoryService.findAll(pageRequest);
        Assertions.assertEquals(5.29, page.getContent().get(9).getProductPrice());
        Assertions.assertEquals(0, page.getNumber());
        Assertions.assertEquals(10, page.getSize());
    }

    @Test
    public void findOneTest() {
        Inventory inventory = inventoryService.findOne(1);
        Assertions.assertEquals("Bacon & Egger", inventory.getProductName());
    }

    @Test
    public void deleteTest() {
        inventoryService.delete(27);
        Assertions.assertEquals(26, inventoryService.findAll().size());
    }

//    @Test
//    public void increaseStock() {
//    }
//
//    @Test
//    public void decreaseStock() {
//    }
}