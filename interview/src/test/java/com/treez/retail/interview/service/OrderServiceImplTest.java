package com.treez.retail.interview.service;

import com.treez.retail.interview.dto.OrderDTO;
import com.treez.retail.interview.enums.OrderStatusEnum;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * @ClassName OrderServiceImplTest
 * @Description OrderServiceImplTest
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class OrderServiceImplTest {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private InventoryServiceImpl inventoryService;

    @Test
    public void create() {
        HashMap<Integer, Integer> cart = new HashMap<>();

        cart.put(4, 56);
        cart.put(12, 18);
        cart.put(25, 23);

        Double expectedAmount = inventoryService.findOne(4).getProductPrice() * 56 +
                                inventoryService.findOne(12).getProductPrice() * 18 +
                                inventoryService.findOne(25).getProductPrice() * 23;

        OrderDTO orderDTO = orderService.create("bill@123.com", cart);

        Assertions.assertEquals("bill@123.com", orderDTO.getEmailAddress());
        Assertions.assertEquals(expectedAmount, orderDTO.getOrderAmount());
        Assertions.assertEquals(3, orderDTO.getOrderDetailList().size());
        Assertions.assertEquals(OrderStatusEnum.getMsgByCode(0), orderDTO.getOrderStatus());
    }

    @Test
    public void findAll() {
        Page<OrderDTO> page = orderService.findAll(new PageRequest(0, 1));
        Assertions.assertEquals(1, page.getContent().size());
        page = orderService.findAll(new PageRequest(0, 2));
        Assertions.assertEquals(2, page.getContent().size());
        page = orderService.findAll(new PageRequest(0, 3));
        Assertions.assertEquals(2, page.getContent().size());
        Assertions.assertEquals(2, page.getTotalElements());
    }

    @Test
    public void findOne() {
        OrderDTO orderDTO = orderService.findOne("1580141637915868423");
        Assertions.assertEquals(12.57, orderDTO.getOrderAmount());
    }

    @Test
    public void update() {
        OrderDTO orderDTO = orderService.findOne("1580141637915868423");
        HashMap<Integer, Integer> cart = new HashMap<>();
        cart.put(4, 56);
        cart.put(12, 18);
        cart.put(25, 23);
        Double expectedAmount = inventoryService.findOne(4).getProductPrice() * 56 +
                inventoryService.findOne(12).getProductPrice() * 18 +
                inventoryService.findOne(25).getProductPrice() * 23;
        orderDTO = orderService.update(orderDTO.getOrderId(), cart);
        Assertions.assertEquals(expectedAmount, orderDTO.getOrderAmount());
    }

    @Test
    public void cancel() {
        orderService.cancel("1580141637915868423");
        OrderDTO orderDTO = orderService.findOne("1580141637915868423");
        Assertions.assertEquals(OrderStatusEnum.getMsgByCode(2), orderDTO.getOrderStatus());
    }

    @Test
    public void delete() {
        orderService.delete("1580141637915868423");
        Long size = orderService.findAll(new PageRequest(0, 1)).getTotalElements();
        Assertions.assertEquals(1, size);
    }
}