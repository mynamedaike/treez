package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.OrderDetail;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName OrderDetailDAOTest
 * @Description OrderDetailDAOTest
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class OrderDetailDAOTest {

    @Autowired
    private OrderDetailDAO orderDetailDAO;

    @Test
    public void saveTest() {
        OrderDetail orderDetail = new OrderDetail("123", "12345678", 1,
                "Coffee", 1.99, 4);
        orderDetail = orderDetailDAO.save(orderDetail);
        Assertions.assertEquals(1.99, orderDetail.getProductPrice());
    }

    @Test
    public void findAllTest() {
        PageRequest pageRequest = new PageRequest(0, 2);
        Page<OrderDetail> page = orderDetailDAO.findAll(pageRequest);
        Assertions.assertEquals(6, page.getTotalElements());
        Assertions.assertEquals(6, orderDetailDAO.findAll().size());
        Assertions.assertEquals("Coffee", page.getContent().get(1).getProductName());
    }

    @Test
    public void findOneTest() {
        OrderDetail orderDetail = orderDetailDAO.findOne("1580141637915813057");
        Assertions.assertEquals(1.99, orderDetail.getProductPrice());
    }

    @Test
    public void deleteTest() {
        orderDetailDAO.delete("1580141872584707994");
        Assertions.assertEquals(5, orderDetailDAO.findAll().size());
    }

//    @Test
//    public void findByOrderIdTest() {
//    }
}