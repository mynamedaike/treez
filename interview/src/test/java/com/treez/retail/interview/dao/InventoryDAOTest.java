package com.treez.retail.interview.dao;

import com.treez.retail.interview.model.Inventory;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName InventoryDAOTest
 * @Description InventoryDAOTest
 * @Author Kevin Dai
 * @Date 2020-01-27
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class InventoryDAOTest {

    @Autowired
    private InventoryDAO inventoryDAO;

    @Test
    public void createTest() {
        Inventory inventory = new Inventory("2% Milk", "2% Milk",
                2.99, 999);
        inventory = inventoryDAO.save(inventory);
        Assertions.assertEquals("2% Milk", inventory.getProductName());

        inventory = inventoryDAO.findOne(18);
        inventory.setProductPrice(6.99);
        inventoryDAO.save(inventory);
        inventory = inventoryDAO.findOne(18);
        Assertions.assertEquals(6.99, inventory.getProductPrice());
    }

    @Test
    public void findAllTest() {
        PageRequest pageRequest = new PageRequest(0,10);
        Page<Inventory> page = inventoryDAO.findAll(pageRequest);
        Assertions.assertEquals(5.29, page.getContent().get(9).getProductPrice());
        Assertions.assertEquals(0, page.getNumber());
        Assertions.assertEquals(10, page.getSize());
    }

    @Test
    public void findOneTest() {
        Inventory inventory = inventoryDAO.findOne(1);
        Assertions.assertEquals("Bacon & Egger", inventory.getProductName());
    }

    @Test
    public void deleteTest() {
        inventoryDAO.delete(20);
        Assertions.assertEquals(26, inventoryDAO.findAll().size());
    }
}